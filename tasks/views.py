from django.shortcuts import render, redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            receipt = form.save()
            receipt.save()
            return redirect("list_projects")
    else:
        form = TaskForm()

    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_project(request, id):
    project = Task.objects.filter(id=id)
    context = {
        "project": project,
    }
    return render(request, "tasks/detail.html", context)


@login_required
def show_my_tasks(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {
        "task_list": task_list,
    }
    return render(request, "tasks/list.html", context)


# Create your views here.
